<?php

require_once __DIR__ . '/vendor/autoload.php';

$params = \OK\PhpTest\ParameterBag::get();

$importer = OK\PhpTest\Importer\ImporterFactory::get($params['i']);
$serializer = OK\PhpTest\Serializer\SerializerFactory::get($params['o']);

$test = new OK\PhpTest\PhpTest($importer, $serializer);
$data = $test->process($params['p'], $params['s']);

if ($params['f'] === 'file') {
    return file_put_contents('output.' . $params['o'], $data);
}

return $data;
