# PHP test task

### Installation

```sh
git clone https://OlegKochetkov999@bitbucket.org/OlegKochetkov999/phptest.git
cd phptest/
composer install
composer dump-autoload
```
### Tests
```sh
vendor/bin/phpunit tests/
```

### Using
There is `file.csv` in root directory for testing
```sh
php cli.php -pfile.csv
```

### Information
You can use input [flags](https://bitbucket.org/OlegKochetkov999/phptest/src/master/src/ParameterBag.php) for manage script's parameters
