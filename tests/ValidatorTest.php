<?php

namespace Tests;

use Tests\TestCase;
use OK\PhpTest\Validator;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class ValidatorTest extends TestCase
{
    /**
     * @dataProvider isValidUTF8StringProvider
     */
    public function testIsValidUTF8String($path, $result)
    {
        $isValidUTF8String = $this->makeCallable(Validator::class, 'isValidUTF8String');
        
        $this->assertEquals($result, $isValidUTF8String->invokeArgs(null, [$path]));
    }
    
    public function isValidUTF8StringProvider()
    {
        return [
            ['test1', true],
            [iconv('UTF-8', 'Windows-1252', 'Côte d\'Ivoire'), false],
            ['\xDF\xDF\xDF', true],
        ];
    }

    /**
     * @dataProvider isValidUrlProvider
     */
    public function testIsValidUrl($url, $result)
    {
        $isValidUrl = $this->makeCallable(Validator::class, 'isValidUrl');
        
        $this->assertEquals($result, $isValidUrl->invokeArgs(null, [$url]));
    }
    
    public function isValidUrlProvider()
    {
        return [
            ['https://yandex.ru', true],
            ['yandex.ru', true],
            ['file://yandex.ru', false],
            ['https://yandexblabla.ru', false]
        ];
    }
    
    
    /**
     * @dataProvider isValidStarsProvider
     */
    public function testIsValidStars($stars, $result)
    {
        $isValidStars = $this->makeCallable(Validator::class, 'isValidStars');
        
        $this->assertEquals($result, $isValidStars->invokeArgs(null, [$stars]));
    }
    
    public function isValidStarsProvider()
    {
        return [
            [-1, false],
            [0, true],
            [3, true],
            [5, true],
            [6, false]
        ];
    }
}
