<?php

namespace Tests;

/** 
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class TestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * Make private and protected function callable
     *
     * @param mixed $object
     * @param string $function
     * @return \ReflectionMethod
     */
    protected function makeCallable($object, $function)
    {
        $method = new \ReflectionMethod($object, $function);
        $method->setAccessible(true);

        return $method;
    }
}

