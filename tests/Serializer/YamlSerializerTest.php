<?php

namespace Tests\Serializer;

use Tests\TestCase;
use OK\PhpTest\Serializer\YamlSerializer;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class YamlSerializerTest extends TestCase
{
    /**
     * @dataProvider serializeProvider
     */
    public function testSerialize($input, $output)
    {
        $serializer = new YamlSerializer();

        $this->assertEquals($output, $serializer->serialize($input));
    }
    
    public function serializeProvider()
    {
        return [
            [
                [
                    [
                        "name" => "test", 
                        "url" => "https://yandex.ru",
                        "stars" => "1"
                    ]
                ], 
                '-
    name: test
    url: \'https://yandex.ru\'
    stars: \'1\'
'
            ]
        ];
    }
}
