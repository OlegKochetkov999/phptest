<?php

namespace Tests\Serializer;

use Tests\TestCase;
use OK\PhpTest\Serializer\JsonSerializer;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class JsonSerializerTest extends TestCase
{
    /**
     * @dataProvider serializeProvider
     */
    public function testSerialize($input, $output)
    {
        $serializer = new JsonSerializer();

        $this->assertEquals($output, $serializer->serialize($input));
    }
    
    public function serializeProvider()
    {
        return [
            [
                [
                    [
                        "name" => "test", 
                        "url" => "https://yandex.ru",
                        "stars" => "1"
                    ],
                    [
                        "name" => "atest", 
                        "url" => "https://yandex.ru",
                        "stars" => "1"
                    ]
                ], 
                '[{"name":"test","url":"https:\/\/yandex.ru","stars":"1"},{"name":"atest","url":"https:\/\/yandex.ru","stars":"1"}]'
            ],
            [
                [
                    [
                        "name" => "test", 
                        "url" => "https://yandex.ru",
                        "stars" => "1"
                    ]
                ], 
                '[{"name":"test","url":"https:\/\/yandex.ru","stars":"1"}]'
            ]
        ];
    }
}
