<?php

namespace Tests\Importer;

use Tests\TestCase;
use OK\PhpTest\Exception\SerializerFactoryException;
use OK\PhpTest\Serializer\JsonSerializer;
use OK\PhpTest\Serializer\YamlSerializer;
use OK\PhpTest\Serializer\SerializerFactory;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class SerializerFactoryTest extends TestCase
{
    /**
     * @dataProvider getProvider
     */
    public function testGet($type, $result)
    {
        if ($result === null) {
            $this->expectException(SerializerFactoryException::class);
        }
        
        $data = SerializerFactory::get($type);
        
        if ($result !== null) {
            $this->assertTrue(get_class($data) === $result);
        }
    }
    
    public function getProvider()
    {
        return [
            ['json', JsonSerializer::class],
            ['yaml', YamlSerializer::class],
            ['invalid', null]
        ];
    }
}
