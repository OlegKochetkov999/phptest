<?php

namespace OK\PhpTest;

use Tests\TestCase;
use OK\PhpTest\ParameterBag;
use OK\PhpTest\Exception\ParameterBagException;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class ParameterBagTest extends TestCase
{
    public function testGetWithoutPath()
    {
        $this->expectException(ParameterBagException::class);

        ParameterBag::get();
    }
}
