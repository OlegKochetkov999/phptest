<?php

namespace Tests\Importer;

use Tests\TestCase;
use OK\PhpTest\Exception\ImporterFactoryException;
use OK\PhpTest\Importer\CsvImporter;
use OK\PhpTest\Importer\ImporterFactory;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class ImporterFactoryTest extends TestCase
{
    /**
     * @dataProvider getProvider
     */
    public function testGet($type, $result)
    {
        if ($result === null) {
            $this->expectException(ImporterFactoryException::class);
        }
        
        $data = ImporterFactory::get($type);
        
        if ($result !== null) {
            $this->assertTrue(get_class($data) === $result);
        }
    }
    
    public function getProvider()
    {
        return [
            ['csv', CsvImporter::class],
            ['invalid', null]
        ];
    }
}
