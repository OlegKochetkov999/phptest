<?php

namespace OK\PhpTest;

use OK\PhpTest\Importer\ImporterInterface;
use OK\PhpTest\Serializer\SerializerInterface;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class PhpTest
{
    /**
     * @var ImporterInterface 
     */
    private $importer;

    /**
     * @var SerializerInterface 
     */
    private $serializer;
    
    /**
     * @param ImporterInterface $importer
     * @param SerializerInterface $serializer
     */
    public function __construct(ImporterInterface $importer, SerializerInterface $serializer)
    {
        $this->importer = $importer;
        $this->serializer = $serializer;
    }

    /**
     * @param string $path
     * @param string|null $sortBy
     *
     * @return string
     */
    public function process(string $path, string $sortBy = null): string
    {
        $data = $this->importer->get($path);

        if ($sortBy and in_array($sortBy, ImporterInterface::STRUCTURE)) {
            usort($data, function ($a, $b) use ($sortBy) {
                $elements = [$a[$sortBy], $b[$sortBy]];
                sort($elements);

                return reset($elements) === $a[$sortBy] ? -1 : 1;
            });
        }

        return $this->serializer->serialize($data);
    }
}