<?php

namespace OK\PhpTest;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class Validator
{
    /**
     * @param array $array
     * @return bool
     */
    public static function isValidData(array $array): bool
    {
        return (self::isValidUTF8String($array[0]) && self::isValidUrl($array[1]) && self::isValidStars($array[2]));
    }

    /**
     * https://www.w3.org/International/questions/qa-forms-utf-8.en
     * @param string $string
     * @return bool
     */
    private static function isValidUTF8String(string $string): bool
    {
        return preg_match('%^(?:
            [\x09\x0A\x0D\x20-\x7E]              # ASCII
            | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
            | \xE0[\xA0-\xBF][\x80-\xBF]         # excluding overlongs
            | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
            | \xED[\x80-\x9F][\x80-\xBF]         # excluding surrogates
            | \xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3
            | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
            | \xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16
            )*$%xs', $string) === 1;
    }

    /**
     * @param string $url
     * @return bool
     */
    private static function isValidUrl(string $url): bool
    {
        $scheme = parse_url($url, PHP_URL_SCHEME);

        if ($scheme === null) {
            $url = 'https://' . $url;
            $scheme = 'https';
        }

        if (!in_array($scheme, ['http', 'https'])) {
            return false;
        }

        return (false !== @file_get_contents($url, false, null, 0, 1));
    }

    /**
     * @param int $stars
     * @return bool
     */
    private static function isValidStars(int $stars): bool
    {
        return ($stars >= 0 && $stars <= 5);
    }
}
