<?php

namespace OK\PhpTest\Serializer;

use OK\PhpTest\Exception\SerializerFactoryException;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class SerializerFactory
{
    /**
     * @param string $type
     *
     * @return SerializerInterface
     * @throws SerializerFactoryException
     */
    public static function get(string $type): SerializerInterface
    {
        $serializerName = __NAMESPACE__ . '\\' . ucfirst($type) . 'Serializer';

        if (!class_exists($serializerName)) {
            throw new SerializerFactoryException(printf('Serializer %s doesn\'t exist', $serializerName));
        }
        
        return new $serializerName();
    }
}
