<?php

namespace OK\PhpTest\Serializer;

use Symfony\Component\Yaml\Yaml;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class YamlSerializer implements SerializerInterface
{
    /**
     * @param array $data
     * @return string
     */
    public function serialize(array $data): string
    {
        return $yaml = Yaml::dump($data);
    }
}
