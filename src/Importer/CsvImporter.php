<?php

namespace OK\PhpTest\Importer;

use OK\PhpTest\Validator;
use OK\PhpTest\Exception\ImporterException;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class CsvImporter implements ImporterInterface
{
    /**
     * @param string $path
     * @return array
     * @throws ImporterException
     */
    public function get(string $path): array
    {
        if (!is_file($path)) {
            throw new ImporterException('Invalid file path ' . $path);
        }

        $handle = fopen($path, "r");

        if ($handle === false) {
            throw new ImporterException('File can\'t be open');
        }

        $keysRow = fgetcsv($handle, 512, ';');
        $keys = [];
        $result = [];
        
        foreach (ImporterInterface::STRUCTURE as $value) {
            $keys[$value] = array_search($value, $keysRow);
        }

        while (($row = fgetcsv($handle, 1000, ";")) !== FALSE) {
            $data = [];
            $dataForValidation = [];
            foreach ($keys as $key => $value) {
                $data[$key] = $dataForValidation[] = $row[$value];
            }

            if (Validator::isValidData($dataForValidation)) {
                $result[] = $data;
            }
        }

        fclose($handle);

        return $result;
    }
}
