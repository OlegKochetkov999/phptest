<?php

namespace OK\PhpTest\Importer;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
interface ImporterInterface
{
    public const STRUCTURE = ['name', 'url', 'stars'];
    
    /**
     * @param string $path
     *
     * @return array
     */
    public function get(string $path): array;
}
