<?php

namespace OK\PhpTest\Importer;

use OK\PhpTest\Exception\ImporterFactoryException;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class ImporterFactory
{
    /**
     * @param string $type
     *
     * @return ImporterInterface
     * @throws ImporterFactoryException
     */
    public static function get(string $type): ImporterInterface
    {
        $importerName = __NAMESPACE__ . '\\' . ucfirst($type) . 'Importer';

        if (!class_exists($importerName)) {
            throw new ImporterFactoryException(printf('Importer %s doesn\'t exist', $importerName));
        }
        
        return new $importerName();
    }
}
