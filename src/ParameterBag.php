<?php

namespace OK\PhpTest;

use OK\PhpTest\Exception\ParameterBagException;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class ParameterBag
{
    /**
     * p - path
     * i - input type [csv]
     * o - output type [json, yaml]
     * s - sort by field [fields from ImporterInterface::STRUCTURE]
     * f - form of output [raw, file]
     */
    public const PARAMS = 'p:i:o:s:f:';

    /**
     * @return array
     * @throws ParameterBagException
     */
    public function get(): array
    {
        $params = getopt(self::PARAMS);

        if (!isset($params['p'])) {
            throw new ParameterBagException('Path doesn\'t exist');
        }

        if (!isset($params['i'])) {
            $params['i'] = 'csv';
        }

        if (!isset($val['o'])) {
            $params['o'] = 'json';
        }

        if (!isset($val['s'])) {
            $params['s'] = null;
        }
        
        if (!isset($val['f'])) {
            $params['f'] = 'file';
        }
        
        return $params;
    }
}
