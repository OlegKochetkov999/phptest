<?php

namespace OK\PhpTest\Exception;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class ParameterBagException extends \Exception
{
}
